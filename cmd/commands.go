package main

import (
	"context"
	"io"
	"os"
	"regexp"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"

	"gitlab.com/glmt/glmt/internal/glmt"
)

func createMR(cmd *cobra.Command, logger zerolog.Logger, out io.StringWriter) {
	flags := cmd.Flags()
	cfg, err := finalConfig(flags)
	if err != nil {
		panic(err)
	}

	ll, err := parseLogLevel(flags)
	if err != nil {
		_, _ = out.WriteString("Failed to parse log level: " + err.Error() + "\n")
		os.Exit(1)
	}

	logger = logger.Level(ll)
	ctx := logger.WithContext(context.Background())

	dryRun, err := flags.GetBool("dryrun")
	if err != nil {
		_, _ = out.WriteString("Failed to parse dryrun: " + err.Error() + "\n")
		os.Exit(1)
	}

	core := createCore(dryRun, out, &cfg.GitLab)

	br, err := regexp.Compile(cfg.MR.BranchRegexp)
	if err != nil {
		_, _ = out.WriteString("Failed to compile branch regexp: " + err.Error() + "\n")
		os.Exit(1)
	}

	params := glmt.CreateMRParams{
		TargetBranch:        cfg.MR.TargetBranch,
		BranchRegexp:        br,
		TitleTemplate:       cfg.MR.Title,
		DescriptionTemplate: cfg.MR.Description,
		Squash:              cfg.MR.Squash,
		RemoveBranch:        cfg.MR.RemoveSourceBranch,
	}

	mr, err := core.CreateMR(ctx, params)
	if err != nil {
		_, _ = out.WriteString("Failed to create MR: " + err.Error() + "\n")
		os.Exit(1)
	}

	_, _ = out.WriteString("MR created\n")
	_, _ = out.WriteString(mr.URL + "\n")
}
